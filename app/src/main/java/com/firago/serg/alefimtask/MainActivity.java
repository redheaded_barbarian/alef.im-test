package com.firago.serg.alefimtask;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    private PictureListAdapter adapter;
    RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final MainViewModule model = ViewModelProviders.of(this).get(MainViewModule.class);
        swipeRefreshLayout = findViewById(R.id.swipeRefresh);

        if (savedInstanceState == null) {
            // start app
            model.loadUrls();
            swipeRefreshLayout.setRefreshing(true);
        }

        setupRecycler();


        adapter = new PictureListAdapter();
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                model.loadUrls();
            }
        });

        model.getUrls().observe(this, new Observer<List<String>>() {
            @Override
            public void onChanged(@Nullable List<String> strings) {
                swipeRefreshLayout.setRefreshing(false);
                adapter.swap(strings);
            }
        });

        model.getError().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                swipeRefreshLayout.setRefreshing(false);
                showErrorDialog(s);
            }
        });

    }

    private void showErrorDialog(String error) {
        ErrorDialogFragment.getInstanse(error).show(getSupportFragmentManager(), "ERROR_DIALOG");
    }

    private void setupRecycler() {
        recyclerView = findViewById(R.id.rvPicture);
        int orientation = getResources().getConfiguration().orientation;

        RecyclerView.LayoutManager layoutManager;
        int spanCount;
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            spanCount = 2;
        } else {
            spanCount = 3;
        }
        layoutManager = new GridLayoutManager(this, spanCount);
        recyclerView.setLayoutManager(layoutManager);
    }


}
