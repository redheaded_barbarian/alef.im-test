package com.firago.serg.alefimtask;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.firago.serg.alefimtask.util.SingleLiveEvent;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainViewModule extends ViewModel {

    public static final String SITE = "http://dev-tasks.alef.im/task-m-001/list.php";

    private final MutableLiveData<List<String>> urls = new MutableLiveData<List<String>>();

    public LiveData<List<String>> getUrls() {
        return urls;
    }

    private final SingleLiveEvent<String> error = new SingleLiveEvent<>();

    public SingleLiveEvent<String> getError() {
        return error;
    }

    public void loadUrls() {
        getUrls(SITE);
    }


    private void getUrls(String site) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(site).build();
        client
                .newCall(request)
                .enqueue(
                        new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                error.postValue(e.getMessage());
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                try {
                                    if (response.isSuccessful()) {
                                        List<String> list = parsingJson(response.body().string());
                                        urls.postValue(list);
                                    } else {
                                        error.postValue(response.message());
                                    }
                                }catch (Exception e){
                                    error.postValue(e.getMessage());
                                }
                            }
                        }

                );
    }

    private List<String> parsingJson(String json) {
        TypeToken<List<String>> stringsToken = new TypeToken<List<String>>() {
        };
        return new Gson().fromJson(json, stringsToken.getType());
    }
}
