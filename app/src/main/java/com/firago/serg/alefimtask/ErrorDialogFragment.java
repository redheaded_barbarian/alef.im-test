package com.firago.serg.alefimtask;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class ErrorDialogFragment extends DialogFragment {
    private static final String ARG_MESSAGE = "com.firago.serg.alefimtask.ErrorDialogFragment";
    public static ErrorDialogFragment getInstanse(String message){
        Bundle bundle = new Bundle();
        bundle.putString(ARG_MESSAGE, message);
        ErrorDialogFragment dialog = new ErrorDialogFragment();
        dialog.setArguments(bundle);
        return dialog;
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String message = getArguments().getString(ARG_MESSAGE);
        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.error_title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .create();
        return dialog;
    }
}
