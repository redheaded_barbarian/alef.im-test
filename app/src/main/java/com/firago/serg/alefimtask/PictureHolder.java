package com.firago.serg.alefimtask;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

class PictureHolder extends RecyclerView.ViewHolder {
    public ImageView ivPicture;
    public String url;
    public void bind(String url){
        Picasso.get().load(url).fit()
                .error(R.drawable.ic_error_picture)
                .placeholder(R.drawable.ic_place_holder_picture)
                .into(ivPicture);
        this.url = url;

    }
    public PictureHolder(View itemView) {
        super(itemView);
        ivPicture = itemView.findViewById(R.id.ivPicture);
    }
}
